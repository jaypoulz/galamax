# Galamax
## Overview
This is a simple remake of the classic "Galaga" game, written with the pygame libs.
It has been modified with the addition of Markov Decision Processes and QLearning.

## Authors
Written by: RJ Marsan
Original Creator: Derek Mcdonald

Modifiers: Shauna Thompson, Max Markmanrud, & Jeremy Poulin


## Instructions:

### Ubuntu/Debian: 
  sudo apt-get install python python-pygame  
  sudo pip install enum34  
  Run ./pylaga.py

### Other Linux:
  Install python, pygame, and enum34  
  python pylaga.py 

import random
import util
from globalvars import Action, Move

class QLearningAgent():

    def __init__(self, world, epsilon, alpha, discount, actionRepeat = 5):
        self.values = util.Counter()  # A Counter is a dict with default 0
        self.world = world
        self.epsilon = epsilon        # Explore rate
        self.alpha = alpha            # Learning rate
        self.discount = discount
        self.actionRepeat = actionRepeat
        self.slow = world.parent.getSlow()

    def getQValue(self, state, action, quad):
        return self.values[state, action, quad[0], quad[1], quad[2], quad[3]]

    def getInput(self):
        # @Max do the thing
        state = self.world.getPlayerPos()
        threats = self.findThreats(self.world.getBaddies())
        return self.getActionsList(state, threats)

    def computeValueFromQValues(self, state, quad):
        actions = self.world.getActionsForState(state)

        if len(actions) == 0:
            return 0.0

        maxVal = float("-infinity")

        for a in actions:
            qval = self.getQValue(state, a, quad)
            if qval > maxVal:
                maxVal = qval

        return maxVal

    def computeActionFromQValues(self, state, quad):

        actions = self.world.getActionsForState(state)
        action = None
        maxVal = float("-infinity")

        for a in actions:
            qval = self.getQValue(state, a, quad)
            if qval > maxVal:
                maxVal = qval
                action = a
        if self.slow:
            print "Action ", action.getMove(), ", shoot =", action.willShoot, " is best move for state ", state, " with Qval ", maxVal
        return action

    def getActionsList(self, state, quad):
        actions = list()
        a = self.getAction(state, quad)
        for i in range(0, self.actionRepeat):
            actions.append(a)
        return actions

    def getAction(self, state, quad):
        # Pick Action
        legalActions = self.world.getActionsForState(state)
        if len(legalActions) == 0:
            return None

        explore = util.flipCoin(self.epsilon)

        if explore == 1:
            return random.choice(legalActions)
        else:
            return self.computeActionFromQValues(state, quad)

    def update(self, state, action, nextState, reward, quad):
        alpha = self.alpha
        forget = (1 - alpha)
        discount = self.discount

        selfQ = self.getQValue(state, action, quad)
        nextQ = self.computeValueFromQValues(nextState, quad)

        term1 = selfQ*forget
        term2 = alpha*(reward + discount*nextQ)

        value = (term1 + term2)

        self.values[state, action, quad[0], quad[1], quad[2], quad[3]] = value

    def getPolicy(self, state):
        return self.computeActionFromQValues(state)

    def getValue(self, state, quad):
        return self.computeValueFromQValues(state, quad)

    def getQuad(self, world):
        return self.findThreats(self.world.getBaddies())

    def findThreats(self, baddies):
        x,y = self.world.getGrid()
        xcenter = x / 2
        ycenter = y / 2

        quad = [0, 0, 0, 0]
        # QUADRANTS
        #  1    0
        #  2    3

        for baddyx,baddyy in baddies:
            if baddyx < xcenter:      # left
                if baddyy < ycenter:  # top left
                    quad[1] = quad[1] + 1
                else:                 # bottom left
                    quad[2] = quad[2] + 1
            else:                     # right
                if baddyy < ycenter:  # top right
                    quad[0] = quad[0] + 1
                else:                 # bottom right
                    quad[3] = quad[3] + 1

        for n in range(0, len(quad)):
            shelf = 0
            while quad[n] > shelf:
                shelf += 5
            quad[n] = shelf

        return quad

#!/usr/bin/env python
#Galaga VERSION .05!
#!/usr/bin/env python
#2007-04-1 RJ Marsan
#Galamax
#Original: 2007-02-20 Derek Mcdonald
#Main class
#################################################################################################################
#
#
#	This is the main class. the class that superceedes all other classes.
#	Its short and sweet but thats the point
#
#general exception handler
#(because if theres an error in any of these imports itll just die w/o warning)
def exception_handler():
    import traceback,sys
    type, info, trace = sys.exc_info()
    tracetop = traceback.extract_tb(trace)[-1]
    tracetext = 'File %s, Line %d' % tracetop[:2]
    if tracetop[2] != '?':
        tracetext += ', Function %s' % tracetop[2]
    exception_message = '%s:\n%s\n\n%s\n"%s"'
    message = exception_message % (str(type), str(info), tracetext, tracetop[3])
    if type not in (KeyboardInterrupt, SystemExit):
        print message
    raise

#import pygame os and sys libraries
try:
  import pygame, os, sys, math, random
  from pygame.locals import*
  import globalvars
  from bullet import Bullet, EnemyBullet
  from background import BackgroundManager, bgstars
  from enemy import Enemy, EnemyManager
  from qlearning import QLearningAgent
  from player import Player
  from stage import Stage
  from display import *
  from menu import Menu
  from game import Gamelolz
  from menulists import MenuLists, menulists
except:
  exception_handler()
  sys.exit(0)

if not pygame.font: print 'Warning, fonts disabled'
#################################################################################################################
#
#
#  simple class to manage the entire game..... it helps with organization
#
#
class galamax:
  def __init__(self):
    alpha = 0.5
    epsilon = 0.5
    self.slowMode = False
    self.game=Gamelolz(self)
    self.menu=menulists
    self.agent=QLearningAgent(self.game, epsilon, alpha, 0, self.game.moveloop)
    count = 1
    self.menu.init_menu(self.game)

    while True:
        if count % 5 == 0:
            print "Starting game using learned policy..."
            self.agent.alpha = 0
            self.agent.epsilon = 0
        self.game.start()
        if self.slowMode:
            self.menu.init_menu(self.game)
        if count % 5 == 0:
            self.agent.alpha = alpha
            self.agent.epsilon = epsilon
        count += 1

    while self.menu.exit_menu(self.game):
      self.game.start()

  def getSlow(self):
      return self.slowMode

  def getAgent(self):
    return self.agent
##the one line that starts the game
#if __name__ == "__main__":
#  lolz=galamax()
lolz=galamax()

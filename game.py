###
# 2014-10-23 Jeremy Poulin
# Galamax
# 2007-04-1 RJ Marsan
# Pylaga
# Original: 2007-02-20 Derek Mcdonald
# Subclass of pylaga.py
###
#
#
#	arguably the most important class, this is the game object
#	it *is* the game. in an object.
#
# import pygame os and sys libraries
try:
 import pygame, os, sys, math, random, time
 from pygame.locals import*
 from globalvars import*
 from bullet import Bullet, EnemyBullet
 from background import BackgroundManager, bgstars
 from collections import deque
 from enemy import Enemy, EnemyManager
 from player import Player
 from qlearning import QLearningAgent
 from stage import Stage
 from display import *
 from menu import Menu
 from menulists import MenuLists,menulists
 import ecollision
except:
 print "A File Was Missing. CHECK!"

if not pygame.font: print 'Warning, fonts disabled'

###
# Now for the actual game class
class Gamelolz:
 # This is the __init__
 # its important.
 def __init__(self,parent):
  self.parent=parent
  globalvars.asdf = 0
  self.lagcount=0
  self.leftkeydown=0
  self.moveloop = 3
  self.rightkeydown=0
  self.enemylist = []
  self.list_enemys = EnemyManager()
  self.stage=Stage(self.list_enemys,globalvars.player_list)
  self.list_allie_shots = pygame.sprite.RenderUpdates()
  self.enemy_shots = pygame.sprite.RenderUpdates()
  self.inputhandlers = {}
  self.defaulthandlers = {}
  self.devmode = 0
  self.aaqueue = []
  self.oldPos = None
  self.quad = None
  self.oldScore = 0

  # Builds the default event handler
  self.setdefaulthandlers()

  # @Shauna/Max
  # This is where you should load a new control module
  # See function for details
  self.loadinputmodule(self.aicontrols())

 # Clears all the variables
 def clear_vars(self):
  self.leftkeydown=0
  self.rightkeydown=0
  health.set_health(globalvars.max_health)
  points.set_points(0)
  shots.set_shots(0)
  hits.set_hits(0)
  globalvars.x=400
  globalvars.y=globalvars.WIN_RESY-60
  self.stage.set_stage(-1) #hax
  globalvars.enemy_bullet_odds=100
  self.list_enemys.empty()
  self.list_allie_shots.empty()
  globalvars.player_list.empty()
  self.enemy_shots.empty()

 # Define function to draw player ship on X, Y plane
 def pship(self, x,y):
  globalvars.player_list.clear(globalvars.surface,globalvars.screen)
  self.enemylist+=globalvars.player_list.draw(globalvars.surface)

 # Define function to move the enemy ship
 def emove(self):
  self.list_enemys.clear(globalvars.surface, globalvars.screen)
  self.enemylist+=self.list_enemys.draw(globalvars.surface)

 # Draws all the enemys you ask it
 def draw_enemys(self):
  # Some recursive loops:
  for enemycol in range(self.stage.get_stage()[0]):
   # Now for the rows
   for enemyrow in range(self.stage.get_stage()[1]):
    # Make a new enemy object:
    tempenemy=Enemy(self.list_enemys)
    tempenemy.set_pos(globalvars.xmin+enemycol*(globalvars.enemy_width+globalvars.enemy_spacing_x),globalvars.ymin+enemyrow*(globalvars.enemy_height+globalvars.enemy_spacing_y)-150)
    tempenemy.set_range(globalvars.xmin+enemycol*(globalvars.enemy_width+globalvars.enemy_spacing_x),globalvars.xmax-(self.stage.get_stage()[0]-enemycol)*(globalvars.enemy_height+globalvars.enemy_spacing_x))

    # Now add the temp enemy to the array and we're good to go
    self.list_enemys.add(tempenemy)

 # So I'm trying out having the program check for collisions, instead of the
 # enemy objects I think I might switch to the objects, but still keep this
 # function just hand the computing to the object
 def test_collision(self):
  todie=pygame.sprite.groupcollide(self.list_enemys, self.list_allie_shots,0,0)
  for enemy,bullet in todie.iteritems():
   self.list_allie_shots.remove(bullet)
   enemy.set_state(0)
   points.add_points(10)
   hits.add_hits(1)
  if pygame.sprite.spritecollideany(self.player, self.enemy_shots):
   self.player.set_hit()
   health.hit()

 # If there are no enemys left, go to the next stage
 def check_done(self):
  if not self.list_enemys:
   self.stage.next_stage()
   self.draw_enemys()

 #checks to see if we can expand the ranges of the bots so its nice and.... umm... nice.
 def check_rows(self):
  if globalvars.asdf % 20==0:
   #simple sorting algorithm to find the highest values
   highest=globalvars.xmin
   lowest=globalvars.xmax
   for enemy in self.list_enemys:
    if enemy.get_range()[1] > highest:
     highest=enemy.get_range()[1]
    if enemy.get_range()[0] < lowest:
     lowest=enemy.get_range()[0]
   highest=globalvars.xmax-highest
   lowest=lowest-globalvars.xmin
   if highest != 0 or lowest != 0: #makes things |--| this much more efficient
    for enemy in self.list_enemys:
     erange=enemy.get_range()
     enemy.set_range(erange[0]-lowest,erange[1]+highest)

 #major hack just to get this thing playable..... sorry
 def again(self):
  if health.get_health() <= 0:
   return False
  return True

 #this is called if the player shoots
 def pshoot(self, sx, sy):
  self.player.shoot(self.list_allie_shots,sx,sy)
  shots.add_shots(1)

 #draws the bullet.... duh. come on dude.
 def drawbullets(self):
  #for x in self.list_allie_shots:
   #x.draw()
  self.list_allie_shots.clear(globalvars.surface,globalvars.screen)
  self.enemy_shots.clear(globalvars.surface,globalvars.screen)
  self.enemylist+=self.list_allie_shots.draw(globalvars.surface)
  self.enemylist+=self.enemy_shots.draw(globalvars.surface)

 #...
 def drawsidepanel(self):
  if globalvars.asdf%5==0:
   globalvars.side_panel.update()
  globalvars.side_panel.clear(globalvars.surface,globalvars.screen)
  self.enemylist+=globalvars.side_panel.draw(globalvars.surface)

 # Goes through all the objects and makes each of them move as necessary
 def tick(self):
   self.list_allie_shots.update()
   self.list_enemys.update()
   self.enemy_shots.update()

   # Move player ship
   if self.leftkeydown: self.player.move_one(0)
   if self.rightkeydown: self.player.move_one(1)

 ######################
 #heres a bunch of metafunctions
 #i break it up so its really easy to add new features
 #like if we ant a counter? add something to check() and draw()
 #all of these are called once per frame
 def check(self):
  self.check_done()
  self.test_collision()
  self.check_rows()
  bgstars.update()
  self.list_enemys.shoot(self.enemy_shots)
  self.player.update()

 def draw(self):
  self.enemylist+=bgstars.draw()
  self.enemylist+=bgstars.clear()
  self.drawbullets()
  self.pship(globalvars.x,globalvars.y)
  self.emove()
  self.drawsidepanel()

 #does just what it sounds like.....
 def clear_screen(self):
  globalvars.surface.fill(globalvars.bgcolor)
  pygame.display.flip()

 #for debugging info mostly
 def dispvars(self):
  print "The Enemy Array size is:",len(self.list_enemys.sprites())
  print "The Player Shot Array size is:",len(self.list_allie_shots.sprites())
  print "The Enemy Shot Array size is:",len(self.enemy_shots.sprites())

 # Defines input handlers
 def loadinputmodule(self, handlers):
   self.inputhandlers = handlers

   # Runs through the new module and restore defaults when necessary
   for key in self.defaulthandlers:
     if key in self.inputhandlers:
       continue;
     self.inputhandlers[key] = self.defaulthandlers[key]

 # Listens for input events
 def input(self, events):
  global x
  global y
  pygame.event.pump() # Somewhere in the pygame docs called for this line

  for event in events:
    if event.type in self.inputhandlers:
      if self.inputhandlers[event.type] is not None:
        self.inputhandlers[event.type](event)

    pygame.event.clear()

 # Build default event handlers
 def setdefaulthandlers(self):
   self.defaulthandlers[QUIT] = self.quitgame
   self.defaulthandlers[pygame.MOUSEMOTION] = self.mousemotion
   self.defaulthandlers[pygame.MOUSEBUTTONDOWN] = self.shoot
   self.defaulthandlers[pygame.KEYDOWN] = self.keydownevents
   self.defaulthandlers[pygame.KEYUP] = self.keyupevents

 # AI Player
 def aicontrols(self):
  controls = {}
  controls[pygame.KEYDOWN] = self.gameflowkeyevents
  controls[pygame.KEYUP] = None
  controls[pygame.MOUSEMOTION] = None
  controls[pygame.MOUSEBUTTONDOWN] = None

  return controls

 def gameflowkeyevents(self, event):
  if event.key == pygame.K_ESCAPE:
     self.quitgame(event)
  if event.key == pygame.K_q:
     self.quitgame(event)
  if event.key == pygame.K_p:
     menulists.pause_menu()

 def quitgame(self, event):
   sys.exit(0)

 def mousemotion(self, event):
   pygame.event.get()
   tempx=pygame.mouse.get_pos()[0]-self.player.rect.width/2
   ## Just to make sure we don't get the ship way out there:
   if tempx > globalvars.xmax: #if its outside the globalvars.window, just stick it as far as possible
     self.player.move(globalvars.xmax,globalvars.y)
   elif tempx < globalvars.xmin:
     self.player.move(globalvars.xmin,globalvars.y)
   elif abs(tempx-globalvars.x) > globalvars.smooth_scroll_var1:  #smooth scrolling if the mouse gets far from the ship
     self.player.move(self.player.get_pos().left+(tempx-self.player.get_pos().left)/globalvars.smooth_scroll_var2,globalvars.y)
   else:  # If it gets down to this point,
     # We've passed all sanity checks so just move it
     self.player.move(tempx,globalvars.y)

 def shoot(self, event):
   self.pshoot(self.player.rect.centerx-globalvars.BULLET_WIDTH/2,globalvars.y)

 def keydownevents(self, event):
   if event.key == pygame.K_q:
     self.quitgame(event)
   if event.key == pygame.K_p:
     menulists.pause_menu()
   if event.key == pygame.K_ESCAPE:
     self.quitgame(event)
   # Keyboard controls
   if event.key == pygame.K_LEFT:
     self.leftkeydown=1
   if event.key == pygame.K_RIGHT:
     self.rightkeydown=1
   if event.key == pygame.K_SPACE:
     self.shoot(event)

 def keyupevents(self, event):
   # Keyboard controls
   if event.key == pygame.K_LEFT:
     self.leftkeydown=0
   if event.key == pygame.K_RIGHT:
     self.rightkeydown=0

###
# Control Functions

 def start(self):
  self.clear_vars()
  self.player=Player()
  self.agent=self.parent.getAgent()
  globalvars.player_list.add(self.player)
  self.player.set_pos(globalvars.x,globalvars.y)
  self.loop()

 # Yeah see this one does all of the work
 def loop(self):
  # Start loop
  alpha = self.parent.getAgent().alpha
  epsilon = self.parent.getAgent().epsilon
  while self.again():
   # Refresh globalvars.screen...needs to be done once in a while
   if globalvars.asdf>=globalvars.REFRESH_TIME:
    #self.clear_screen()
    globalvars.asdf=0
   globalvars.asdf+=1

   # Check everythign and see if changes need to be made
   self.check()

   # Draw bullets
   self.draw()

   # Initiate input function
   self.input(pygame.event.get())

   ### Run agent code ###
   # Refresh the agents events we've run out
   if not self.aaqueue:
    self.oldScore = self.getScore()
    self.oldPos = self.getPlayerPos()
    self.quad = self.agent.getQuad(self)
    self.aaqueue = deque(self.agent.getInput())

   # If the agent gave us events, let's perform then
   action = None
   update = False
   if self.aaqueue:
    action = self.aaqueue.popleft()
    self.performaction(action)
    if not self.aaqueue:
     update = True

   # Move everything
   self.tick()

   # Applies the smart screen updating
   pygame.display.update(self.enemylist)
   self.enemylist=[]

   if (update):
    # Update the agent
    self.agent.update(self.oldPos,
                      action,
                      self.getPlayerPos(),
                      self.getScore() - self.oldScore,
                      self.quad)

   # Pauses and waits
   timeittook=globalvars.clock.tick(globalvars.FPS)
   if (self.devmode):
    time.sleep(1)
  print "FINAL SCORE: ", self.getScore()


 def performaction(self, action):
  if action.willShoot:
   self.shoot(None)

  move = action.getMove()
  if move is Move.left:
   self.player.move_one(0)
  elif move is Move.right:
   self.player.move_one(1)

 ##################################
 # API INTERFACE                  #
 ##################################
 def getPlayerPos(self):
  return self.player.get_pos_center()

 def getGrid(self):
  return (globalvars.x, globalvars.y)

 def actionInRange(self, state, move):
  rect = self.player.get_pos().copy()
  offset = 0
  if move is Move.left:
   offset -= 5
  elif move is Move.right:
   offset += 5

  newcenter = (state[0] + offset, state[1])
  rect.center = newcenter
  if gamewindow.contains(rect):
    return True
  return False

 def getActionsForState(self, state):
   validactions = []
   for action in globalvars.ACTIONS:
      if self.actionInRange(state, action.getMove()):
        validactions.append(action)

   return validactions

 def getBaddies(self):
  baddylist = []
  for s in self.enemy_shots:
   baddylist.append(s.rect.center)

  for e in self.list_enemys:
   baddylist.append(e.get_pos())

  return baddylist

 def getShotsFired(self):
  return shots.get_shots()

 def getScore(self):
  return round(self.getAccuracy() * points.get_points()
                    + health.get_health())

 def getAccuracy(self):
  shots = self.getShotsFired()
  if shots == 0:
   return shots
  else:
   return round(float(hits.get_hits()) / float(shots) * 100)
